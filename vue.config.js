const { defineConfig } = require("@vue/cli-service");

const path = require("path");

function resolve(dir) {
  //此处使用path.resolve 或path.join 可自行调整
  return path.join(__dirname, dir);
}

const webpack = require("webpack");
//
const PrerenderSPAPlugin = require("prerender-spa-plugin-next");
//

module.exports = defineConfig({
  transpileDependencies: true,
  //publicPath部署应用包时的基本 URL /部署在根目录，./部署在任意目录？
  publicPath: process.env.NODE_ENV === "production" ? "/" : "/",
  // 输出文件目录
  outputDir: "dist",
  //静态资源路径
  assetsDir: "assets",
  //
  indexPath: "index.html",
  // eslint-loader 是否在保存的时候检查
  lintOnSave: false,

  //默认情况下，生成的静态资源在它们的文件名中包含了 hash 以便更好的控制缓存
  filenameHashing: false,

  // 是否使用包含运行时编译器的 Vue 构建版本
  runtimeCompiler: false,

  // 生产环境是否生成 sourceMap 文件
  productionSourceMap: false,

  // 生成的 HTML 中的 <link rel="stylesheet"> 和 <script> 标签上启用 Subresource Integrity (SRI)
  integrity: false,

  //
  devServer: {
    host: "0.0.0.0",
    port: 8889,
    https: false,
    proxy: {
      "/wordpress/wp-json": {
        target: "http://www.bestoget.com",
        ws: false,
        changeOrigin: true,
        // pathRewrite: {
        //   "^/wp": "/wordpress/wp-json/wp",
        // },
      },
      "/wordpress/archives": {
        target: "http://www.bestoget.com",
        ws: false,
        changeOrigin: true,
      },
    },
    // before: (app) => {},
  },

  // webpack相关配置
  chainWebpack: (config) => {
    //设置别名
    config.resolve.alias.set("@", resolve("src"));
    //    .set('vue$', 'vue/dist/vue.esm.js')
    config.plugin("html").tap((args) => {
      args[0].title = "Besto Industrial Co. Ltd";
      return args;
    });
  },

  configureWebpack: (config) => {
    const plugins = config.plugins;

    if (process.env.NODE_ENV === "production") {
      // 为生产环境修改配置...
      // 生产环境
      config.mode = "production";

      plugins.push(
        new PrerenderSPAPlugin({
          // 这个目录只能有一级，如果目录层次大于一级，在生成的时候不会有任何错误提示，在预渲染的时候只会卡着不动。
          // staticDir: path.join(__dirname, "dist"), // 读取vue-cli已打包文件的根目录 prerender-spa-plugin会在这里开启一个服务
          staticDir: "/spa",
          // outputDir: "/spa", //经过prerender-spa-plugin处理的文件最终保存的地方
          indexPath: "/spa/index.html",
          // 对应自己的路由文件，比如a有参数，就需要写成 /a/param1。
          routes: ["/", "/service", "/profile", "/project", "/blog"],
          //
          renderer: require("@prerenderer/renderer-puppeteer"),
          // 这个很重要，如果没有配置这段，也不会进行预编译
          rendererOptions: {
            // headless: false,
            inject: { foo: "bar" },
            timeout: 10000,
            // headless: false, // Display the browser window when rendering. Useful for debugging.
            // Optional - Wait to render until the specified event is dispatched on the document.
            // eg, with `document.dispatchEvent(new Event('custom-render-trigger'))`
            renderAfterDocumentEvent: "render-event",
            // Automatically block any third-party requests.
            skipThirdPartyRequests: true,
          },
          postProcess(context) {
            // Remove /index.html from the output path if the dir name ends with a .html file extension.
            // For example: /dist/dir/special.html/index.html -> /dist/dir/special.html
            if (context.route.endsWith(".html")) {
              // context.outputPath = path.join(
              //   __dirname,
              //   "dist/spa/",
              //   context.route
              // );
            }
          },
        })
      );
    } else {
      // 为开发环境修改配置...
      // 开发环境
      config.mode = "development";
    }
    //
    config.externals = {
      $: "jquery",
    };

    plugins.push(
      new webpack.ProvidePlugin({
        $: "jquery",
        jQuery: "jquery",
        "window.jQuery": "jquery",
      })
    );
  },

  // css: {
  //   // 是否分离css（插件ExtractTextPlugin）
  //   extract: true,
  //   // 是否开启 CSS source maps
  //   sourceMap: false,
  //   // // 是否启用 CSS modules for all css / pre-processor files.
  //   // modules: false,
  //   requireModuleExtension: false,
  //   //向 CSS 相关的 loader 传递选项,css预设器配置项
  //   // loaderOptions: {
  //   //   // css: {
  //   //   //   // 这里的选项会传递给 css-loader
  //   //   // },
  //   //   // postcss: {
  //   //   //     // 这里的选项会传递给 postcss-loader
  //   //   // }
  //   // },
  // },
  // 构建时开启多进程处理 babel 编译
  parallel: require("os").cpus().length > 1,

  // PWA 插件相关配置
  pwa: {
    iconPaths: {
      favicon64: "./favicon.ico",
      favicon32: "./favicon.ico",
      favicon16: "./favicon.ico",
      appleTouchIcon: "./favicon.ico",
      maskIcon: "./favicon.ico",
      msTileImage: "./favicon.ico",
    },
  },

  //第三方插件
  pluginOptions: {},
});
