





//////////////////////////////////////
<template>
  <div></div>
</template>

<script setup>
import {
  reactive,
  toRefs,
  onBeforeMount,
  onMounted,
  getCurrentInstance,
  defineComponent,
} from "vue";

// const Component = defineComponent({});

const { proxy } = getCurrentInstance();

const props = defineProps({});

const data = reactive({});
const refData = toRefs(data);

onBeforeMount(() => {});
onMounted(() => {});

const methods = {};
</script>
<style lang="scss" scoped></style>
