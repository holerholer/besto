// html转义
export function escapeToHtml(str) {
  let arrEntities = {
    lt: "<",
    gt: ">",
    nbsp: " ",
    amp: "&",
    quot: '"',
  };
  return str.replace(/&(lt|gt|nbsp|amp|quot);/gi, function (all, t) {
    return arrEntities[t];
  });
}

//wordpress转义
export function wordpressDecodeHTML(str) {
  if (!str) {
    return "";
  }
  return str.replace(/&lt;|&gt;|&amp;|&#038;/g, function (matches) {
    return {
      "&lt;": "<",
      "&gt;": ">",
      "&amp;": "&",
      "&#038;": "&",
    }[matches];
  });
}

//
export function deleteHtmlTag(str) {
  if (!str) {
    return "";
  }
  return str.replace(/<[^>]+>/g, "");
}
