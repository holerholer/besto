import axios from "axios";
import { notification } from "ant-design-vue";

let baseUrl = "/wordpress/wp-json";

let isProduct = process.env.NODE_ENV === "production" ? true : false;

// 3. 配置信息
let config = {
  // 每次请求的协议、IP地址。  设置该配置后，每次请求路径都可以使用相对路径，例如"/admin/login"
  baseURL: baseUrl,
  // 请求超时时间
  timeout: 10000,
  // 表示跨域请求时是否需要使用凭证
  withCredentials: true,
};

// 2. 创建实例
const instance = axios.create(config);

instance.defaults.headers.post["Content-Type"] =
  "application/x-www-form-urlencoded";

// 添加请求拦截器
instance.interceptors.request.use(
  function (config) {
    // 在发送请求之前做些什么
    return config;
  },
  function (error) {
    // 对请求错误做些什么
    return Promise.reject(error);
  }
);

// 添加响应拦截器
instance.interceptors.response.use(
  function (response) {
    // 对响应数据做点什么
    if (response.status == 200) {
      return response;
    } else {
      if (isProduct) {
        console.log("接口异常，请检查");
      } else {
        notification.error({
          message: "接口异常，请检查",
          duration: 2,
        });
      }
    }
  },
  function (error) {
    // 对响应错误做点什么
    if (isProduct) {
      console.log("请求异常");
    } else {
      notification.error({
        message: "请求异常",
        duration: 2,
      });
    }

    return Promise.reject(error);
  }
);

export { instance as axios };
