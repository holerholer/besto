import { createRouter, createWebHistory } from "vue-router";
import HomeView from "../views/HomeView.vue";

import HomeLayout from "@/layouts/HomeLayout.vue";
import ContentLayout from "@/layouts/ContentLayout.vue";

const routes = [
  // {
  //   path: "/",
  //   name: "home",
  //   component: HomeView,
  // },
  // {
  //   path: "/about",
  //   name: "about",
  //   // route level code-splitting
  //   // this generates a separate chunk (about.[hash].js) for this route
  //   // which is lazy-loaded when the route is visited.
  //   component: () =>
  //     import(/* webpackChunkName: "about" */ "../views/AboutView.vue"),
  // },
  {
    path: "/",
    name: "home",
    component: HomeLayout,
    children: [{ path: "", component: () => import("@/views/home/Index.vue") }],
    meta: {
      keepAlive: true,
    },
  },
  {
    path: "/service",
    name: "service",
    component: ContentLayout,
    children: [
      {
        path: "",
        name: "serviceIndex",
        component: () => import("@/views/service/Index.vue"),
        meta: { title: "Service", keepAlive: true },
      },
      {
        path: "/service/detail/:id",
        name: "serviceDetail",
        component: () => import("@/views/service/ServiceDetail.vue"),
        meta: {
          keepAlive: false,
          scrollToTop: true,
        },
      },
    ],
  },
  {
    path: "/project",
    name: "project",
    component: ContentLayout,
    children: [
      {
        path: "",
        name: "projectIndex",
        component: () => import("@/views/project/Index.vue"),
        meta: {
          title: "Project",
          keepAlive: true,
          imgUrl: require("@/assets/image/project-banner.jpg"),
        },
      },
      {
        path: "/project/:id",
        component: () => import("@/views/project/Index.vue"),
        meta: {
          title: "Project",
        },
      },
      {
        path: "/project/detail/:id",
        name: "projectDetail",
        component: () => import("@/views/project/ProjectDetail.vue"),
        meta: {
          title: "Project",
          keepAlive: false,
          scrollToTop: true,
          imgUrl: require("@/assets/image/project-banner.jpg"),
        },
      },
    ],
  },
  {
    path: "/product",
    name: "product",
    component: ContentLayout,
    children: [
      {
        path: "",
        name: "ProductIndex",
        component: () => import("@/views/product/Index.vue"),
        meta: {
          title: "Product",
          scrollToTop: true,
          keepAlive: true,
          imgUrl: require("@/assets/image/product-banner.jpg"),
        },
      },
      {
        path: "/product/:id",
        component: () => import("@/views/product/Index.vue"),
        meta: { title: "Product" },
      },
      {
        path: "/product/detail/:id",
        name: "ProductDetail",
        component: () => import("@/views/product/ProductDetail.vue"),
        meta: {
          title: "Product",
          keepAlive: false,
          scrollToTop: true,
          imgUrl: require("@/assets/image/product-banner.jpg"),
        },
      },
    ],
  },
  {
    path: "/blog",
    name: "blog",
    component: ContentLayout,
    children: [
      {
        path: "",
        name: "blogIndex",
        component: () => import("@/views/blog/Index.vue"),
        meta: {
          title: "Blog",
          imgUrl: require("@/assets/image/blog-banner.jpg"),
        },
        redirect: "/blog/list",
        children: [
          {
            path: "/blog/list",
            name: "blogList",
            component: () => import("@/views/blog/BlogList.vue"),
            meta: { title: "Blog", keepAlive: true },
          },
          {
            path: "/blog/detail/:id",
            name: "blogDetail",
            component: () => import("@/views/blog/BlogDetail.vue"),
            meta: { title: "Blog Detail", keepAlive: false, scrollToTop: true },
          },
        ],
      },
    ],
  },
  {
    path: "/profile",
    name: "profile",
    component: ContentLayout,
    redirect: "/profile/intorduce",
    meta: {
      imgUrl: require("@/assets/image/aboutUs-banner.jpg"),
    },
    children: [
      {
        path: "/profile/intorduce",
        name: "profileIntorduce",
        component: () => import("@/views/profile/Introduce.vue"),
        meta: {
          title: "About Us",
        },
      },
      {
        path: "/profile/contact",
        name: "profileContact",
        component: () => import("@/views/profile/ContactUs.vue"),
        meta: {
          title: "Contact Us",
        },
      },
    ],
  },

  {
    path: "/404",
    component: () => import("../views/error/404.vue"),
  },
  {
    path: "/:catchAll(.*)",
    redirect: "/404",
  },
];

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes,
  scrollBehavior(to, from, savedPosition) {
    if (savedPosition) {
      // savedPosition is only available for popstate navigations.
      return savedPosition;
    } else {
      // const position = {};
      // new navigation.
      // scroll to anchor by returning the selector
      // if (to.hash) {
      //   position.selector = to.hash;
      // }
      // check if any matched route config has meta that requires scrolling to top
      // if (to.matched.some((m) => m.meta.scrollToTop)) {
      //   // cords will be used if no selector is provided,
      //   // or if the selector didn't match any element.
      // }
      // if the returned position is falsy or an empty object,
      // will retain current scroll position.
      // console.log("position", position);
      return { left: 0, top: 0 };
    }
  },
});

// router.afterEach((to, from, next) => {
//   document
//     .querySelector("body")
//     .setAttribute("style", "overflow: auto !important;");
// });

export default router;
