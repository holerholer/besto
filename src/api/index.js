import { axios } from "@/utils/request.js";
import qs from "qs";

// 获取栏目列表
export function getCategoryList(params) {
  var url = `/wp/v2/categories`;
  return axios.get(url, {
    params: params,
  });
}

// 获取栏目列表
export function getArticleById(id) {
  var url = `/wp/v2/posts/${id}`;
  return axios.get(url);
}

// 获取文章列表
export function getArticleList(params) {
  var url = `/wp/v2/posts`;
  return axios.get(url, {
    params: params,
  });
}

//获取标签列表
export function getTagList(params) {
  var url = `/wp/v2/tags`;
  return axios.get(url, {
    params: params,
  });
}

//保存联系方式
export function addContact(params) {
  var url = `/contact-form-7/v1/contact-forms/13/feedback`;
  return axios.post(url, qs.stringify(params));
}

//获取图片url
export function getImgUrlFromMediaById(url) {
  if (!url) {
    return null;
  }
  var imageUrl = url.replace("(https?://)[^/]+", "");
  return imageUrl;
}

// iframe自适应
export function iframeAutoFit(iframeObj) {
  setTimeout(function () {
    if (!iframeObj) {
      return;
    }
    var bHeight = iframeObj.contentWindow.document.body.scrollHeight;
    var dHeight = iframeObj.contentWindow.document.documentElement.scrollHeight;
    var height = Math.max(bHeight, dHeight);
    console.log(bHeight, dHeight);
    iframeObj.height = height;
  }, 200);
}

export function toTree(data) {
  // 删除 所有 children,以防止多次调用
  data.forEach(function (item) {
    delete item.children;
  });

  // 将数据存储为 以 id 为 KEY 的 map 索引数据列
  var map = {};
  data.forEach(function (item) {
    map[item.id] = item;
  });
  //        console.log(map);
  var val = [];
  data.forEach(function (item) {
    // 以当前遍历项，的pid,去map对象中找到索引的id
    var parent = map[item.parent];
    // 好绕啊，如果找到索引，那么说明此项不在顶级当中,那么需要把此项添加到，他对应的父级中
    if (parent) {
      (parent.children || (parent.children = [])).push(item);
    } else {
      //如果没有在map中找到对应的索引ID,那么直接把 当前的item添加到 val结果集中，作为顶级
      val.push(item);
    }
  });
  return val;
}
