import Vue from "vue";
import { LANGUAGE } from "@/constants/index.js";

import { createI18n } from "vue-i18n";

//自定义
import localeCN from "./lang/zh-CN.js";
import localeUS from "./lang/en-US.js";
//
import zhCN from "ant-design-vue/es/locale/zh_CN";
import enUS from "ant-design-vue/es/locale/en_US";

import dayjs from "dayjs";
dayjs.locale("en");
import "dayjs/locale/zh-cn";

const messages = {
  en: {
    message: {
      ...localeUS,
      ...enUS,
    },
    hello: "hi there!",
  },
  zh: {
    message: {
      ...localeCN,
      ...zhCN,
    },
    hello: "你好世界",
  },
};

export const defaultLang = "zh-CN";

const i18n = createI18n({
  // 使用 Composition API 模式，则需要将其设置为false
  legacy: false,
  locale: localStorage.getItem(LANGUAGE) || "en", // 语言标识（缓存里面没有就用中文）,
  //配置语言包
  messages: messages,
  //fallbackLocale: defaultLang, //没有英文的时候默认 中文语言
  globalInjection: true, // 全局注入 $t 函数
  silentTranslationWarn: true, //报错时加上这个参数可以取消警告
});

export default i18n;
