const lang = {
  title: "title", //（标题）
  title1: "title1", //（标题2）
  placeholder: "please enter", //请输入中文
  Home: {
    AboutUs: {
      title: "ABOUT US",
      CompanyIntroductionSection1:
        "Besto Industrial Co. Ltd, established in 2000, is a supplier of hotel furniture, home furniture, interior and exterior decoration, ceiling, tiles, lights, building material GRC/EPS, etc.",
      CompanyIntroductionSection2:
        "We have exported more than 100 countries like Philippines, Thailand, Singapore, Malaysia, India, USA, Mexico, Canada, Spain, Russia, Saudi Arabia, etc",
    },
    Service: {
      title: "What we offer?",
      details: [
        {
          iconStyle: "fa-solid fa-briefcase",
          title: "Good quality control",
        },
        {
          iconStyle: "fa-solid fa-filter-circle-dollar",
          title: "Highly competitive prices",
        },
        {
          iconStyle: "fa-brands fa-product-hunt",
          title: "State-of-art technology products",
        },
        {
          iconStyle: "fa-solid fa-sitemap",
          title: "Effective OEM&ODM service",
        },

        {
          iconStyle: "fa-solid fa-comment",
          title: "Smooth communication",
        },
        {
          iconStyle: "fa-solid fa-people-arrows-left-right",
          title: "Best professional team of lifestyle consumer electronics.",
        },
      ],
    },
    Scope: {
      title: "Business Scope",
      details: [
        {
          iconStyle: "fa-solid fa-file-circle-check",
          title: "constructing solution",
        },
        {
          iconStyle: "fa-solid fa-pen-ruler",
          title: "exterior and interior design service",
        },
        {
          iconStyle: "fa-brands fa-product-hunt",
          title: "building material (GRC/GRG/EPS)",
        },
        {
          iconStyle: "fa-solid fa-shield",
          title: "tempered glass",
        },
        {
          iconStyle: "fa-solid fa-building",
          title: "furniture(home, office, hotel, hospital, gaming)",
        },
      ],
    },
  },
};

export default lang;
