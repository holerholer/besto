const lang = {
  title: "标题", //（标题）
  title1: "标题2", //（标题2）
  placeholder: "a", //请输入中文
  Home: {
    AboutUs: {
      title: "关于我们",
      CompanyIntroductionSection1: "Besto Industrial Co. Ltd",
      CompanyIntroductionSection2: "We have exported more than 100 countries",
    },
  },
};

export default lang;
