import { createApp } from "vue";
import App from "./App.vue";
import router from "./router";
import store from "./store";

const app = createApp(App);
app.use(store).use(router);

//国际化
import i18n from "./locales";
app.use(i18n);

//
// import "ant-design-vue/dist/antd.css"; // or 'ant-design-vue/dist/antd.less'

import {
  Result,
  notification,
  Button,
  Pagination,
  Form,
  Input,
  Col,
  Row,
  Image,
} from "ant-design-vue";
app
  .use(Result)
  .use(notification)
  .use(Button)
  .use(Pagination)
  .use(Form)
  .use(Input)
  .use(Col)
  .use(Row)
  .use(Image);

//
import VueSocialSharing from "vue-social-sharing";
app.use(VueSocialSharing);

//
import "bootstrap/dist/css/bootstrap.min.css";
import "bootstrap/dist/js/bootstrap.bundle.min.js";

//全局主题
import "@/assets/css/theme.scss";

import "@/assets/css/wordpress.css";

// 全局方法
import { wordpressDecodeHTML, deleteHtmlTag } from "@/utils/StringUtil";
app.config.globalProperties.$wordpressDecodeHTML = wordpressDecodeHTML;
app.config.globalProperties.$deleteHtmlTag = deleteHtmlTag;

import { getImgUrlFromMediaById, iframeAutoFit } from "@/api/index";
app.config.globalProperties.$getImgUrlFromMediaById = getImgUrlFromMediaById;
app.config.globalProperties.$iframeAutoFit = iframeAutoFit;
//分享组件
// import VueSocialSharing from "vue-social-sharing";
// app.use(VueSocialSharing);

//
app.mount("#app");
