import { APP_LANGUAGE } from "../mutation-types";
import { LANGUAGE } from "@/constants";

const i18n = {
  state: {
    language: "zh",
  },
  mutations: {
    [APP_LANGUAGE](state, lang) {
      // 设置缓存
      localStorage.setItem(LANGUAGE, lang);
      // 修改状态
      state.language = lang;
    },
  },
  actions: {
    setAppLanguage({ commit }, lang) {
      commit(APP_LANGUAGE, lang);
    },
  },
};

export default i18n;
