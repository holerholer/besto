// mutation-types.js
export const APP_LANGUAGE = "APP_LANGUAGE";

//轮播图列表
export const BANNER_LIST = "BANNER_LIST";

//
export const HOME_PROFILE = "HOME_PROFILE";

//项目分类
export const PROJECT_CATEGORY = "PROJECT_CATEGORY";

//
export const PRODUCT_CATEGORY = "PRODUCT_CATEGORY";

//博客分类
export const BLOG_CATEGORY = "BLOG_CATEGORY";

//业务范围分类
export const HOME_BUSINESS_SCOPE_LIST = "HOME_BUSINESS_SCOPE_LIST";

//主页项目列表
export const HOME_PROJECT_LIST = "HOME_PROJECT_LIST";

//主页博客列表
export const HOME_BLOG_LIST = "HOME_BLOG_LIST";

//
export const FOOTER_DATA = "FOOTER_DATA";

export const All_CATEGORY = "All_CATEGORY";
