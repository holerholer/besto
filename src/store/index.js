import { createStore } from "vuex";

import i18n from "./i18n";
import getters from "./getters";

import {
  FOOTER_DATA,
  PROJECT_CATEGORY,
  PRODUCT_CATEGORY,
  BLOG_CATEGORY,
  HOME_BUSINESS_SCOPE_LIST,
  HOME_PROJECT_LIST,
  HOME_BLOG_LIST,
  BANNER_LIST,
  HOME_PROFILE,
  All_CATEGORY,
} from "./mutation-types";

import { getArticleList, getCategoryList, getArticleById } from "@/api";

import { deleteHtmlTag } from "@/utils/StringUtil";

export default createStore({
  state: {
    homeProfile: "",
    //
    bannerList: [],
    //项目分类
    projectCategoryList: [],
    //产品分类
    productCategoryList: [],
    //博客分类
    blogCategoryList: [],
    //服务列表
    homeBusinessScopeList: [],
    //主页项目列表
    homeProjectList: [],
    //主页博客列表
    homeBlogList: [],
    //页脚信息
    footerData: [],
    //
    allCategoryList: [],
  },
  getters: getters,
  mutations: {
    [BANNER_LIST](state, bannerList) {
      state.bannerList = bannerList;
    },
    [HOME_PROFILE](state, homeProfile) {
      state.homeProfile = homeProfile;
    },
    [PROJECT_CATEGORY](state, projectCategoryList) {
      state.projectCategoryList = projectCategoryList;
    },
    [PRODUCT_CATEGORY](state, productCategoryList) {
      state.productCategoryList = productCategoryList;
    },
    [BLOG_CATEGORY](state, blogCategoryList) {
      state.blogCategoryList = blogCategoryList;
    },
    [HOME_BUSINESS_SCOPE_LIST](state, homeBusinessScopeList) {
      state.homeBusinessScopeList = homeBusinessScopeList;
    },
    [HOME_PROJECT_LIST](state, homeProjectList) {
      state.homeProjectList = homeProjectList;
    },
    [HOME_BLOG_LIST](state, homeBlogList) {
      state.homeBlogList = homeBlogList;
    },
    [FOOTER_DATA](state, footerData) {
      state.footerData = footerData;
    },
    [All_CATEGORY](state, allCategoryList) {
      state.allCategoryList = allCategoryList;
    },
  },
  actions: {
    //获取简介
    getHomeProfile({ commit, state }) {
      getArticleById(50).then((res) => {
        var data = res.data;
        commit(HOME_PROFILE, data);
      });
    },
    //获取轮播图
    getBannerList({ commit, state }) {
      var params = {
        categories: 28,
      };
      getArticleList(params).then((res) => {
        var data = res.data;
        commit(BANNER_LIST, data);
      });
    },
    //获取项目类型
    getProjectCategoryList({ commit, state }) {
      var params = {
        parent: 21,
      };
      getCategoryList(params).then((res) => {
        var data = res.data;
        commit(PROJECT_CATEGORY, data);
      });
    },
    //获取项目类型
    getProductCategoryList({ commit, state }) {
      var params = {
        parent: 41,
      };
      getCategoryList(params).then((res) => {
        var data = res.data;
        commit(PRODUCT_CATEGORY, data);
      });
    },
    //获取博客类型
    getBlogCategoryList({ commit, state }) {
      var params = {
        parent: 22,
      };
      getCategoryList(params).then((res) => {
        var data = res.data;
        commit(BLOG_CATEGORY, data);
      });
    },
    //获取服务列表
    getHomeBusinessScopeList({ commit, state }) {
      var params = {
        categories: 31,
        per_page: 99,
      };
      getArticleList(params).then((res) => {
        var data = res.data;
        commit(HOME_BUSINESS_SCOPE_LIST, data);
      });
    },
    //获取8个首页的项目（大类）
    getHomeProjectList({ commit, state }) {
      var params = {
        categories: 34,
        per_page: 4,
        // sticky: true,
      };
      getArticleList(params).then((res) => {
        var data = res.data;
        commit(HOME_PROJECT_LIST, data);
      });
    },
    //获取6个首页的博客（大类）
    getHomeBlogList({ commit, state }) {
      var params = {
        categories: 35,
        per_page: 6,
        // sticky: true,
      };
      getArticleList(params).then((res) => {
        var data = res.data;
        commit(HOME_BLOG_LIST, data);
      });
    },
    //获取页脚信息
    getFooterData({ commit, state }) {
      getArticleById(23).then((res) => {
        var data = res.data;
        var footerData = deleteHtmlTag(data.content.rendered).replace(
          /&#91;/g,
          "["
        );
        commit(FOOTER_DATA, JSON.parse(footerData));
      });
    },
    getAllCategory({ commit, state }) {
      var params = {
        per_page: 99,
      };
      getCategoryList(params).then((res) => {
        var data = res.data;
        commit(All_CATEGORY, data);
      });
    },
    //
    initData({ dispatch }) {
      // proxy.$store.dispatch("getServiceList");
      // proxy.$store.dispatch("getBannerList");
      // proxy.$store.dispatch("getHomeProfile");
      // proxy.$store.dispatch("getHomeProjectList");
      // proxy.$store.dispatch("getHomeBlogList");
      // proxy.$store.dispatch("getBlogCategoryList");
      // proxy.$store.dispatch("getProjectCategoryList");
      // 初始化数据
      dispatch("getBannerList");
      dispatch("getHomeProfile");
      dispatch("getHomeBusinessScopeList");
      dispatch("getHomeProjectList");
      dispatch("getHomeBlogList");
      //
      dispatch("getBlogCategoryList");
      dispatch("getProjectCategoryList");
      dispatch("getProductCategoryList");
      dispatch("getFooterData");
      dispatch("getAllCategory");
    },
  },
  modules: { i18n },
});
