const getters = {
  language: (state) => state.language,
  homeProfile: (state) => state.homeProfile,
  bannerList: (state) => state.bannerList,
  homeBusinessScopeList: (state) => state.homeBusinessScopeList,
  homeProjectList: (state) => state.homeProjectList,
  homeBlogList: (state) => state.homeBlogList,
  blogCategoryList: (state) => state.blogCategoryList,
  projectCategoryList: (state) => state.projectCategoryList,
  productCategoryList: (state) => state.productCategoryList,
  footerData: (state) => state.footerData,
  allCategoryList: (state) => state.allCategoryList,
};

export default getters;
